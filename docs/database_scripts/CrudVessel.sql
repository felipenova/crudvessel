CREATE DATABASE  IF NOT EXISTS `crudvessel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `crudvessel`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: crudvessel
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `refcountry`
--

DROP TABLE IF EXISTS `refcountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refcountry` (
  `cntryGkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `cntryAlpha3Code` varchar(3) NOT NULL,
  `cntryCode` varchar(2) NOT NULL,
  `cntryName` varchar(50) NOT NULL,
  `cntryNum3Code` varchar(3) NOT NULL,
  `cntryOfficialName` varchar(100) NOT NULL,
  PRIMARY KEY (`cntryGkey`),
  UNIQUE KEY `UK_1tuvxhaqwjyffu8araiacu9gs` (`cntryAlpha3Code`),
  UNIQUE KEY `UK_is6ngvwowv2dbi20ol8j0qf39` (`cntryCode`),
  UNIQUE KEY `UK_il39lx2ye0ovx7e6rxjpvylq7` (`cntryName`),
  UNIQUE KEY `UK_rujri7k18f4d5mp1ynfqlie05` (`cntryNum3Code`),
  UNIQUE KEY `UK_2jrb5ncbf2n4rpbscbrti8o0m` (`cntryOfficialName`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refcountry`
--

LOCK TABLES `refcountry` WRITE;
/*!40000 ALTER TABLE `refcountry` DISABLE KEYS */;
INSERT INTO `refcountry` VALUES (1,'AND','AD','Andorra','020','Principality of Andorra'),(2,'AFG','AF','Afghanistan','004','Islamic State of Afghanistan'),(3,'AIA','AI','Anguilla','660','Anguilla'),(4,'ARM','AM','Armenia','051','Republic of Armenia'),(5,'AGO','AO','Angola','024','Republic of Angola'),(6,'ARG','AR','Argentina','032','Argentine Republic'),(7,'AUT','AT','Austria','040','Republic of Austria'),(8,'ABW','AW','Aruba','533','Aruba'),(9,'AZE','AZ','Azerbaijan','031','Republic of Azerbaijan'),(10,'BRB','BB','Barbados','052','Barbados'),(11,'BEL','BE','Belgium','056','Kingdom of Belgium'),(12,'BGR','BG','Bulgaria','100','Republic of Bulgaria'),(13,'BDI','BI','Burundi','108','Republic of Burundi'),(14,'BLM','BL','Saint Barthelemy','652','Collectivity of Saint Barthelemy'),(15,'BRN','BN','Brunei Darussalam','096','Brunei Darussalam'),(16,'BRA','BR','Brazil','076','Federative Republic of Brazil'),(17,'BTN','BT','Bhutan','064','Kingdom of Bhutan'),(18,'BWA','BW','Botswana','072','Republic of Botswana'),(19,'BLZ','BZ','Belize','084','Belize '),(20,'CCK','CC','Cocos (Keeling) Islands','166','Cocos (Keeling) Islands'),(21,'CAF','CF','Central African Republic','140','Central African Republic'),(22,'CHE','CH','Switzerland','756','Swiss Confederation'),(23,'COK','CK','Cook Islands','184','Cook Islands '),(24,'CMR','CM','Cameroon','120','Republic of Cameroon'),(25,'COL','CO','Colombia','170','Republic of Colombia'),(26,'CUB','CU','Cuba','192','Republic of Cuba'),(27,'CXR','CX','Christmas Island','162','Christmas Island'),(28,'CZE','CZ','Czech Republic','203','Czech Republic'),(29,'DJI','DJ','Djibouti','262','Republic of Djibouti'),(30,'DMA','DM','Dominica','212','Commonwealth of Dominica'),(31,'DZA','DZ','Algeria','012','People\'s Democratic Republic of Algeria'),(32,'EST','EE','Estonia','233','Republic of Estonia'),(33,'ESH','EH','Western Sahara','732','Western Sahara'),(34,'ESP','ES','Spain','724','Kingdom of Spain'),(35,'FIN','FI','Finland','246','Republic of Finland'),(36,'FLK','FK','Falkland Islands (Malvinas)','238','Falkland Islands (Malvinas)'),(37,'FRO','FO','Faroe Islands','234','Faroe Islands'),(38,'FXX','FX','France, Metropolitan (reserved)','249','France, Metropolitan (reserved)'),(39,'GBR','GB','United Kingdom','826','United Kingdom of Great Britain and North Ireland'),(40,'GEO','GE','Georgia','268','Republic of Georgia'),(41,'GGY','GG','Guernsey','831','Bailiwick of Guernsey'),(42,'GIB','GI','Gibraltar','292','Gibraltar '),(43,'GMB','GM','Gambia','270','Republic of the Gambia'),(44,'GLP','GP','Guadeloupe','312','Department of Guadeloupe'),(45,'GRC','GR','Greece','300','Hellenic Republic'),(46,'GTM','GT','Guatemala','320','Republic of Guatemala'),(47,'GNB','GW','Guinea-Bissau','624','Republic of Guinea-Bissau'),(48,'HKG','HK','Hong Kong','344','Hong Kong '),(49,'HND','HN','Honduras','340','Republic of Honduras'),(50,'HTI','HT','Haiti','332','Republic of Haiti'),(51,'IDN','ID','Indonesia','360','Republic of Indonesia'),(52,'ISR','IL','Israel','376','State of Israel'),(53,'IND','IN','India','356','Republic of India'),(54,'IRQ','IQ','Iraq','368','Republic of Iraq'),(55,'ISL','IS','Iceland','352','Republic of Iceland'),(56,'JEY','JE','Jersey','832','Bailiwick of Jersey'),(57,'JOR','JO','Jordan','400','Hashemite Kingdom of Jordan'),(58,'KEN','KE','Kenya','404','Republic of Kenya'),(59,'KHM','KH','Cambodia','116','Kingdom of Cambodia'),(60,'COM','KM','Comoros','174','Union of the Comoros'),(61,'PRK','KP','Korea, North','409','Democratic People\'s Republic of Korea'),(62,'KWT','KW','Kuwait','414','State of Kuwait'),(63,'KAZ','KZ','Kazakhstan','398','Republic of Kazakhstan'),(64,'LBN','LB','Lebanon','422','Lebanese Republic'),(65,'LIE','LI','Liechtenstein','438','Principality of Liechtenstein'),(66,'LBR','LR','Liberia','430','Republic of Liberia'),(67,'LTU','LT','Lithuania','440','Republic of Lithuania'),(68,'LVA','LV','Latvia','428','Republic of Latvia'),(69,'MAR','MA','Morocco','504','Kingdom of Morocco'),(70,'MDA','MD','Moldova','498','Republic of Moldova'),(71,'MAF','MF','Saint Martin, French','663','Collectivity of Saint Martin'),(72,'MHL','MH','Marshall Islands','584','Republic of the Marshall Islands'),(73,'MLI','ML','Mali','466','Republic of Mali'),(74,'MNG','MN','Mongolia','496','Mongolia'),(75,'MNP','MP','Northern Mariana Islands','580','Commonwealth of the Northern Mariana Islands'),(76,'MRT','MR','Mauritania','478','Islamic Republic of Mauritania'),(77,'MLT','MT','Malta','470','Republic of Malta'),(78,'MDV','MV','Maldives','462','Republic of Maldives'),(79,'MEX','MX','Mexico','484','United Mexican States'),(80,'MOZ','MZ','Mozambique','508','Republic of Mozambique'),(81,'NCL','NC','New Caledonia','540','New Caledonia'),(82,'NFK','NF','Norfolk Island','574','Norfolk Island'),(83,'NIC','NI','Nicaragua','558','Republic of Nicaragua'),(84,'NOR','NO','Norway','578','Kingdom of Norway'),(85,'NRU','NR','Nauru','520','Republic of Nauru'),(86,'NZL','NZ','New Zealand','554','New Zealand'),(87,'PAN','PA','Panama','591','Republic of Panama'),(88,'PYF','PF','French Polynesia','258','French Polynesia'),(89,'PHL','PH','Philippines','608','Republic of the Philippines'),(90,'POL','PL','Poland','616','Republic of Poland'),(91,'PCN','PN','Pitcairn','612','Pitcairn'),(92,'PSE','PS','Palestinian Territory, Occupied','275','Occupied Palestinian Territory'),(93,'PLW','PW','Palau','585','Republic of Palau'),(94,'QAT','QA','Qatar','634','State of Qatar'),(95,'ROU','RO','Romania','642','Romania'),(96,'RUS','RU','Russian Federation','643','Russian Federation '),(97,'SAU','SA','Saudi Arabia','682','Kingdom of Saudi Arabia'),(98,'SYC','SC','Seychelles','690','Republic of Seychelles'),(99,'SWE','SE','Sweden','752','Kingdom of Sweden'),(100,'SHN','SH','Saint Helena','654','Saint Helena '),(101,'SJM','SJ','Svalbard and Jan Mayen','744','Svalbard and Jan Mayen'),(102,'SLE','SL','Sierra Leone','694','Republic of Sierra Leone'),(103,'SEN','SN','Senegal','686','Republic of Senegal'),(104,'SUR','SR','Suriname','740','Republic of Suriname'),(105,'SLV','SV','El Salvador','222','Republic of El Salvador'),(106,'SWZ','SZ','Swaziland','748','Kingdom of Swaziland'),(107,'TCD','TD','Chad','148','Republic of Chad'),(108,'TGO','TG','Togo','768','Togolese Republic'),(109,'TJK','TJ','Tajikistan','762','Republic of Taiikistan'),(110,'TLS','TL','Timor-Leste','626','Democratic Republic of Timor-Leste'),(111,'TUN','TN','Tunisia','788','Republic of Tunisia'),(112,'TUR','TR','Turkey','792','Republic of Turkey'),(113,'TUV','TV','Tuvalu','798','Tuvalu'),(114,'TZA','TZ','Tanzania','834','United Republic of Tanzania'),(115,'UGA','UG','Uganda','800','Republic of Uganda'),(116,'USA','US','United States','840','United States of America'),(117,'UZB','UZ','Uzbekistan','860','Republic of Uzbekistan'),(118,'VCT','VC','Saint Vincent & the Grenadines','670','Saint Vincent & the Grenadines'),(119,'VGB','VG','Virgin Islands (British)','092','Virgin Islands (British) '),(120,'VNM','VN','Viet Nam','704','Socialist Republic of Viet Nam'),(121,'WLF','WF','Wallis and Futuna Islands','876','Wallis and Futuna Islands'),(122,'YEM','YE','Yemen','887','Republic of Yemen'),(123,'ZAF','ZA','South Africa','710','Republic of South Africa'),(124,'ZWE','ZW','Zimbabwe','716','Republic of Zimbabwe'),(125,'ZMB','ZM','Zambia','894','Republic of Zambia'),(126,'ARE','AE','United Arab Emirates','784','United Arab Emirates'),(127,'ATG','AG','Antigua and Barbuda','028','Antigua and Barbuda'),(128,'ALB','AL','Albania','008','Republic of Albania'),(129,'ANT','AN','Netherlands Antilles','530','Netherlands Antilles'),(130,'ATA','AQ','Antarctica','010','Antarctica'),(131,'ASM','AS','American Samoa','016','American Samoa'),(132,'AUS','AU','Australia','036','Australia '),(133,'ALA','AX','Aland Islands','248','Aland Islands'),(134,'BIH','BA','Bosnia and Herzegovina','070','Republic of Bosnia and Herzegovina'),(135,'BGD','BD','Bangladesh','050','People\'s Republic of Bangladesh'),(136,'BFA','BF','Burkina Faso','854','Burkina Faso '),(137,'BHR','BH','Bahrain','048','Kingdom of Bahrain'),(138,'BEN','BJ','Benin','204','Republic of Benin'),(139,'BMU','BM','Bermuda','060','Bermuda'),(140,'BOL','BO','Bolivia','068','Republic of Bolivia'),(141,'BHS','BS','Bahamas','044','Commonwealth of the Bahamas'),(142,'BVT','BV','Bouvet Island','074','Bouvet Island'),(143,'BLR','BY','Belarus','112','Republic of Belarus'),(144,'CAN','CA','Canada','124','Canada '),(145,'COD','CD','Republic of Congo','180','The Democratic Republic of the Congo'),(146,'COG','CG','Congo','178','Republic of the Congo'),(147,'CIV','CI','Ivory Coast','384','Republic of Ivory Coast'),(148,'CHL','CL','Chile','152','Republic of Chile'),(149,'CHN','CN','China','156','People\'s Republic of China'),(150,'CRI','CR','Costa Rica','188','Republic of Costa Rica'),(151,'CPV','CV','Cape Verde','132','Republic of Cape Verde'),(152,'CYP','CY','Cyprus','196','Republic of Cyprus'),(153,'DEU','DE','Germany','276','Federal Republic of Germany'),(154,'DNK','DK','Denmark','208','Kingdom of Denmark'),(155,'DOM','DO','Dominican Republic','214','Dominican Republic '),(156,'ECU','EC','Ecuador','218','Republic of Ecuador'),(157,'EGY','EG','Egypt','818','Arab Republic of Egypt'),(158,'ERI','ER','Eritrea','232','Eritrea'),(159,'ETH','ET','Ethiopia','271','Ethiopia'),(160,'FJI','FJ','Fiji','242','Republic of the Fiji Islands'),(161,'FSM','FM','Micronesia','583','Federated States of Micronesia'),(162,'FRA','FR','France','250','French Republic'),(163,'GAB','GA','Gabon','266','Gabonese Republic'),(164,'GRD','GD','Grenada','308','Grenada'),(165,'GUF','GF','French Guiana','254','Department of Guiana'),(166,'GHA','GH','Ghana','288','Republic of Ghana'),(167,'GRL','GL','Greenland','304','Greenland '),(168,'GIN','GN','Guinea','324','Republic of Guinea'),(169,'GNQ','GQ','Equatorial Guinea','226','Republic of Equatorial Guinea'),(170,'SGS','GS','South Georgia/So Sandwich Isl','239','South Georgia and the South Sandwich Islands'),(171,'GUM','GU','Guam','316','Guam'),(172,'GUY','GY','Guyana','328','Republic of Guyana'),(173,'HMD','HM','Heard Island & McDonald Isl','334','Heard Island & McDonald Islands'),(174,'HRV','HR','Croatia','191','Republic of Croatia'),(175,'HUN','HU','Hungary','348','Republic of Hungary'),(176,'IRL','IE','Ireland','372','Ireland'),(177,'IMN','IM','Isle of Man','833','Isle of Man'),(178,'IOT','IO','British Indian Ocean Territory','086','British Indian Ocean Territory'),(179,'IRN','IR','Iran','364','Islamic Republic of Iran'),(180,'ITA','IT','Italy','380','Italian Republic'),(181,'JAM','JM','Jamaica','388','Jamaica'),(182,'JPN','JP','Japan','392','Japan'),(183,'KGZ','KG','Kyrgyzstan','417','Kyrgyz Republic'),(184,'KIR','KI','Kiribati','296','Republic of Kiribati'),(185,'KNA','KN','Saint Kitts And Nevis','659','Saint Kitts And Nevis '),(186,'KOR','KR','Korea, South','410','Republic of Korea'),(187,'CYM','KY','Cayman Islands','136','Cayman Islands'),(188,'LAO','LA','Laos','418','Lao People\'s Democratic Republic'),(189,'LCA','LC','Saint Lucia','662','Saint Lucia'),(190,'LKA','LK','Sri Lanka','144','Democratic Socialist Republic of Sri Lanka'),(191,'LSO','LS','Lesotho','426','Kingdom of Lesotho'),(192,'LUX','LU','Luxembourg','442','Grand Duchy of Luxembourg'),(193,'LBY','LY','Libyan Arab Jamahiriya','434','Socialist People\'s Libyan Arab Jamahiriya'),(194,'MCO','MC','Monaco','492','Principality of Monaco'),(195,'MNE','ME','Montenegro','499','Montenegro'),(196,'MDG','MG','Madagascar','450','Republic of Madagascar'),(197,'MKD','MK','Macedonia','807','The Former Yugoslav Republic of Macedonia'),(198,'MMR','MM','Myanmar','104','Union of Myanmar'),(199,'MAC','MO','Macao','446','Macao Special Administrative Region of China'),(200,'MTQ','MQ','Martinique','474','Department of Martinique'),(201,'MSR','MS','Montserrat','500','Montserrat'),(202,'MUS','MU','Mauritius','480','Republic of Mauritius'),(203,'MWI','MW','Malawi','454','Republic of Malawi'),(204,'MYS','MY','Malaysia','458','Malaysia'),(205,'NAM','NA','Namibia','516','Republic of Namibia'),(206,'NER','NE','Niger','562','Republic of the Niger'),(207,'NGA','NG','Nigeria','566','Federal Republic of Nigeria'),(208,'NLD','NL','Netherlands','528','Kingdom of the Netherlands'),(209,'NPL','NP','Nepal','524','Federal Democratic Republic of Nepal'),(210,'NIU','NU','Niue','570','Republic of Niue'),(211,'OMN','OM','Oman','512','Sultanate of Oman'),(212,'PER','PE','Peru','604','Republic of Peru'),(213,'PNG','PG','Papua New Guinea','598','Papua New Guinea'),(214,'PAK','PK','Pakistan','586','Islamic Republic of Pakistan'),(215,'SPM','PM','Saint Pierre And Miquelon','666','Territorial Collectivity of St. Pierre & Miquelon'),(216,'PRI','PR','Puerto Rico','630','Puerto Rico'),(217,'PRT','PT','Portugal','620','Portuguese Republic'),(218,'PRY','PY','Paraguay','600','Republic of Paraguay'),(219,'REU','RE','Reunion','638','Department of Reunion'),(220,'SRB','RS','Serbia','688','Republic of Serbia'),(221,'RWA','RW','Rwanda','646','Rwandese Republic'),(222,'SLB','SB','Solomon Islands','090','Solomon Islands '),(223,'SDN','SD','Sudan','736','Republic of the Sudan'),(224,'SGP','SG','Singapore','702','Republic of Singapore'),(225,'SVN','SI','Slovenia','705','Republic of Slovenia'),(226,'SVK','SK','Slovakia','703','Slovak Republic'),(227,'SMR','SM','San Marino','674','Republic of San Marino'),(228,'SOM','SO','Somalia','706','Somali Republic'),(229,'STP','ST','Sao Tome and Principe','678','Democratic Republic of Sao Tome and Principe'),(230,'SYR','SY','Syrian Arab Republic','760','Syrian Arab Republic'),(231,'TCA','TC','Turks and Caicos Islands','796','Turks and Caicos Islands '),(232,'ATF','TF','French Southern Territories','260','French Southern Territories '),(233,'THA','TH','Thailand','734','Kingdom of Thailand'),(234,'TKL','TK','Tokelau','772','Tokelau'),(235,'TKM','TM','Turkmenistan','795','Turkmenistan '),(236,'TON','TO','Tonga','776','Kingdom of Tonga'),(237,'TTO','TT','Trinidad and Tobago','780','Republic of Trinidad and Tobago'),(238,'TWN','TW','Taiwan','158','Taiwan, Province of China'),(239,'UKR','UA','Ukraine','804','Ukraine'),(240,'UMI','UM','US Minor Outlying Islands','581','US Minor Outlying Islands'),(241,'URY','UY','Uruguay','858','Eastern Republic of Uruguay'),(242,'VAT','VA','Vatican City State','336','Vatican City State '),(243,'VEN','VE','Venezuela','862','Bolivarian Republic of Venezuela'),(244,'VIR','VI','Virgin Islands (U.S.)','850','Virgin Islands of the United States'),(245,'VUT','VU','Vanuatu','548','Republic of Vanuatu'),(246,'WSM','WS','Samoa','882','Independent State of Western Samoa'),(247,'MYT','YT','Mayotte','175','Territorial Collectivity of Mayotte');
/*!40000 ALTER TABLE `refcountry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scopedbizunit`
--

DROP TABLE IF EXISTS `scopedbizunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scopedbizunit` (
  `bzuGkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `bzuId` varchar(50) NOT NULL,
  `bzuName` varchar(255) NOT NULL,
  `bzuRole` varchar(255) NOT NULL,
  PRIMARY KEY (`bzuGkey`),
  UNIQUE KEY `UK_4gsc7wgwyir6qncq4xhx8qrqi` (`bzuId`),
  UNIQUE KEY `UK_4repoysicfwuao81ka6e8f7h8` (`bzuName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scopedbizunit`
--

LOCK TABLES `scopedbizunit` WRITE;
/*!40000 ALTER TABLE `scopedbizunit` DISABLE KEYS */;
INSERT INTO `scopedbizunit` VALUES (2,'CLI','CLIPPER SHIPPING LINES','LINEOP'),(3,'COL','COLUMBUS LINE','LINEOP'),(4,'NIV','NIVER LINES','LINEOP'),(5,'DEL','DELMAS','LINEOP');
/*!40000 ALTER TABLE `scopedbizunit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vessel`
--

DROP TABLE IF EXISTS `vessel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessel` (
  `vesGkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `vesCaptain` varchar(35) DEFAULT NULL,
  `vesChanged` datetime DEFAULT NULL,
  `vesChanger` varchar(50) DEFAULT NULL,
  `vesCreated` datetime NOT NULL,
  `vesCreator` varchar(50) NOT NULL,
  `vesDocumentationNbr` varchar(10) DEFAULT NULL,
  `vesId` varchar(8) NOT NULL,
  `vesIsActive` tinyint(1) NOT NULL,
  `vesLloydsId` varchar(8) NOT NULL,
  `vesName` varchar(35) NOT NULL,
  `vesNotes` varchar(50) DEFAULT NULL,
  `vesRadioCallSign` varchar(10) NOT NULL,
  `vesServiceRegistryNbr` varchar(10) DEFAULT NULL,
  `vesStowageScheme` varchar(50) NOT NULL,
  `vesTemperatureUnits` varchar(50) NOT NULL,
  `vesUnitSystem` varchar(50) NOT NULL,
  `cntryGkey` bigint(20) NOT NULL,
  `bzuGkey` bigint(20) NOT NULL,
  `vesclassGkey` bigint(20) NOT NULL,
  PRIMARY KEY (`vesGkey`),
  UNIQUE KEY `UK_b1cvfdqntfvxb3geifbk2qdyw` (`vesId`),
  UNIQUE KEY `UK_5owk8cvsw1a7wawpl9g0yyomt` (`vesLloydsId`),
  UNIQUE KEY `UK_rfgn2k3kpe3eg893dipwf0hyv` (`vesName`),
  UNIQUE KEY `UK_bp6b0wf1sku2arx5g4pvfhew9` (`vesRadioCallSign`),
  KEY `FKkqrk9agodoajegy2yuebt1g6q` (`cntryGkey`),
  KEY `FKmbhb4qchq8mkp79msd6i6tovk` (`bzuGkey`),
  KEY `FKrca356ak96cwp2yf4xp12s32y` (`vesclassGkey`),
  CONSTRAINT `FKkqrk9agodoajegy2yuebt1g6q` FOREIGN KEY (`cntryGkey`) REFERENCES `refcountry` (`cntryGkey`),
  CONSTRAINT `FKmbhb4qchq8mkp79msd6i6tovk` FOREIGN KEY (`bzuGkey`) REFERENCES `scopedbizunit` (`bzuGkey`),
  CONSTRAINT `FKrca356ak96cwp2yf4xp12s32y` FOREIGN KEY (`vesclassGkey`) REFERENCES `vesselclass` (`vesclassGkey`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vessel`
--

LOCK TABLES `vessel` WRITE;
/*!40000 ALTER TABLE `vessel` DISABLE KEYS */;
/*!40000 ALTER TABLE `vessel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vesselclass`
--

DROP TABLE IF EXISTS `vesselclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vesselclass` (
  `vesclassGkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `vesclassBaysAft` bigint(20) DEFAULT NULL,
  `vesclassBaysForward` bigint(20) DEFAULT NULL,
  `vesclassBowOverhangCm` bigint(20) DEFAULT NULL,
  `vesclassBridgeToBowCm` bigint(20) DEFAULT NULL,
  `vesclassChanged` datetime DEFAULT NULL,
  `vesclassChanger` varchar(50) DEFAULT NULL,
  `vesclassCreated` datetime NOT NULL,
  `vesclassCreator` varchar(50) NOT NULL,
  `vesclassGrossRegTon` double DEFAULT NULL,
  `vesclassId` varchar(8) NOT NULL,
  `vesclassIsActive` tinyint(1) NOT NULL,
  `vesclassIsSelfSustaining` tinyint(1) NOT NULL,
  `vesclassLoaCm` bigint(20) NOT NULL,
  `vesclassName` varchar(35) NOT NULL,
  `vesclassNetRegTon` double DEFAULT NULL,
  `vesclassNotes` varchar(50) DEFAULT NULL,
  `vesclassSternOverhangCm` bigint(20) DEFAULT NULL,
  `vesclassVesselType` varchar(50) NOT NULL,
  PRIMARY KEY (`vesclassGkey`),
  UNIQUE KEY `UK_5omdd8sqqgvk9slqp58abggnr` (`vesclassId`),
  UNIQUE KEY `UK_jlqjlbhj5toh1gb029st72qmr` (`vesclassName`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vesselclass`
--

LOCK TABLES `vesselclass` WRITE;
/*!40000 ALTER TABLE `vesselclass` DISABLE KEYS */;
INSERT INTO `vesselclass` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,'2016-06-12 09:00:00','zilics',NULL,'AAME',1,1,16800,'ALI.AAME.ALIANCA AMERICA',NULL,NULL,NULL,'CELL'),(2,NULL,NULL,NULL,NULL,NULL,NULL,'2016-06-12 09:30:00','zilics',NULL,'ABRI',1,0,26000,'KLI.ABRI.ALVSBORG BRIDGE',NULL,NULL,NULL,'CELL'),(3,NULL,NULL,NULL,NULL,NULL,NULL,'2016-06-12 09:31:00','zilics',NULL,'CENT',1,0,22500,'CEC.CENT.CENTURION',NULL,NULL,NULL,'BULK');
/*!40000 ALTER TABLE `vesselclass` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-13 21:36:53
