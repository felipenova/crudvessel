package com.navis.crudvessel.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.navis.crudvessel.vesellenum.VesselTypeEnum;

@Entity
@Table(name="VesselClass") 
@NamedQueries(value={
		@NamedQuery(name="search.vesselclass.all", 
				query="select vc from VesselClass vc"),
		@NamedQuery(name="search.vesselclass.by.gkey", 
		query="select vc from VesselClass vc where vc.vesclassGkey = :gkey")
})
public class VesselClass implements Serializable{

	
	private static final long serialVersionUID = -2287938548935652500L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long vesclassGkey;

	@NotNull
	@Size(max=35)
	@Column(name="vesclassName",nullable=false,length=35,unique=true)
	private String vesclassName;
	
	@NotNull
	@Size(max=8)
	@Column(name="vesclassId",nullable=false,length=8,unique=true)
	private String vesclassId;
	
	@NotNull
	@Column(name="vesclassVesselType",nullable=false)
	@Enumerated(EnumType.STRING)
	private VesselTypeEnum vesclassVesselType;
	
	@NotNull
	@Column(name="vesclassIsActive",nullable=false)
	private Boolean vesclassIsActive;
	
	@NotNull
	@Column(name="vesclassIsSelfSustaining",nullable=false)
	private Boolean vesclassIsSelfSustaining;
	
	@NotNull
	@Column(name="vesclassLoaCm",nullable=false)
	private Long vesclassLoaCm;
	
	@Column(name="vesclassBaysForward",nullable=true)
	private Long vesclassBaysForward;
	
	@Column(name="vesclassBaysAft",nullable=true)
	private Long vesclassBaysAft;
	
	@Column(name="vesclassBowOverhangCm",nullable=true)
	private Long vesclassBowOverhangCm;
	
	@Column(name="vesclassSternOverhangCm",nullable=true)
	private Long vesclassSternOverhangCm;
	
	@Column(name="vesclassBridgeToBowCm",nullable=true)
	private Long vesclassBridgeToBowCm;
	
	@Column(name="vesclassGrossRegTon",nullable=true)
	private Double vesclassGrossRegTon;
	
	@Column(name="vesclassNetRegTon",nullable=true)
	private Double vesclassNetRegTon;
	
	@Size(max=50)
	@Column(name="vesclassNotes",nullable=true,length=50)
	private String vesclassNotes;
	
	@NotNull
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="vesclassCreated",nullable=false)
	private Date vesclassCreated;
	
	@NotNull
	@Size(max=50)
	@Column(name="vesclassCreator",nullable=false,length=50)
	private String vesclassCreator;
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="vesclassChanged",nullable=true)
	private Date vesclassChanged;
	
	@Size(max=50)
	@Column(name="vesclassChanger",nullable=true,length=50)
	private String vesclassChanger;
	
	public VesselClass(){}

	public Long getVesclassGkey() {
		return vesclassGkey;
	}

	public void setVesclassGkey(Long vesclassGkey) {
		this.vesclassGkey = vesclassGkey;
	}

	public String getVesclassName() {
		return vesclassName;
	}

	public void setVesclassName(String vesclassName) {
		this.vesclassName = vesclassName;
	}

	public String getVesclassId() {
		return vesclassId;
	}

	public void setVesclassId(String vesclassId) {
		this.vesclassId = vesclassId;
	}

	public VesselTypeEnum getVesclassVesselType() {
		return vesclassVesselType;
	}

	public void setVesclassVesselType(VesselTypeEnum vesclassVesselType) {
		this.vesclassVesselType = vesclassVesselType;
	}

	public Boolean getVesclassIsActive() {
		return vesclassIsActive;
	}

	public void setVesclassIsActive(Boolean vesclassIsActive) {
		this.vesclassIsActive = vesclassIsActive;
	}

	public Boolean getVesclassIsSelfSustaining() {
		return vesclassIsSelfSustaining;
	}

	public void setVesclassIsSelfSustaining(Boolean vesclassIsSelfSustaining) {
		this.vesclassIsSelfSustaining = vesclassIsSelfSustaining;
	}

	public Long getVesclassLoaCm() {
		return vesclassLoaCm;
	}

	public void setVesclassLoaCm(Long vesclassLoaCm) {
		this.vesclassLoaCm = vesclassLoaCm;
	}

	public Long getVesclassBaysForward() {
		return vesclassBaysForward;
	}

	public void setVesclassBaysForward(Long vesclassBaysForward) {
		this.vesclassBaysForward = vesclassBaysForward;
	}

	public Long getVesclassBaysAft() {
		return vesclassBaysAft;
	}

	public void setVesclassBaysAft(Long vesclassBaysAft) {
		this.vesclassBaysAft = vesclassBaysAft;
	}

	public Long getVesclassBowOverhangCm() {
		return vesclassBowOverhangCm;
	}

	public void setVesclassBowOverhangCm(Long vesclassBowOverhangCm) {
		this.vesclassBowOverhangCm = vesclassBowOverhangCm;
	}

	public Long getVesclassSternOverhangCm() {
		return vesclassSternOverhangCm;
	}

	public void setVesclassSternOverhangCm(Long vesclassSternOverhangCm) {
		this.vesclassSternOverhangCm = vesclassSternOverhangCm;
	}

	public Long getVesclassBridgeToBowCm() {
		return vesclassBridgeToBowCm;
	}

	public void setVesclassBridgeToBowCm(Long vesclassBridgeToBowCm) {
		this.vesclassBridgeToBowCm = vesclassBridgeToBowCm;
	}

	public Double getVesclassGrossRegTon() {
		return vesclassGrossRegTon;
	}

	public void setVesclassGrossRegTon(Double vesclassGrossRegTon) {
		this.vesclassGrossRegTon = vesclassGrossRegTon;
	}

	public Double getVesclassNetRegTon() {
		return vesclassNetRegTon;
	}

	public void setVesclassNetRegTon(Double vesclassNetRegTon) {
		this.vesclassNetRegTon = vesclassNetRegTon;
	}

	public String getVesclassNotes() {
		return vesclassNotes;
	}

	public void setVesclassNotes(String vesclassNotes) {
		this.vesclassNotes = vesclassNotes;
	}

	public Date getVesclassCreated() {
		return vesclassCreated;
	}

	public void setVesclassCreated(Date vesclassCreated) {
		this.vesclassCreated = vesclassCreated;
	}

	public String getVesclassCreator() {
		return vesclassCreator;
	}

	public void setVesclassCreator(String vesclassCreator) {
		this.vesclassCreator = vesclassCreator;
	}

	public Date getVesclassChanged() {
		return vesclassChanged;
	}

	public void setVesclassChanged(Date vesclassChanged) {
		this.vesclassChanged = vesclassChanged;
	}

	public String getVesclassChanger() {
		return vesclassChanger;
	}

	public void setVesclassChanger(String vesclassChanger) {
		this.vesclassChanger = vesclassChanger;
	}
}
