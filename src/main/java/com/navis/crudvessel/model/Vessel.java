package com.navis.crudvessel.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.navis.crudvessel.vesellenum.StowageSchemeEnum;
import com.navis.crudvessel.vesellenum.TempUnitEnum;
import com.navis.crudvessel.vesellenum.UnitSystemEnum;

@Entity
@Table(name="Vessel") 
@NamedQueries(value={
		@NamedQuery(name="search.vessel.all", 
		query="select v from Vessel v"),
		@NamedQuery(name="search.vessel.by.gkey", 
		query="select v from Vessel v where v.vesGkey = :gkey"),
		@NamedQuery(name="search.vessel.by.id.or.name", 
		query="select v from Vessel v where upper(v.vesId) = :search or upper(v.vesName) = :search")
})
public class Vessel implements Serializable{

	private static final long serialVersionUID = 2298496846865861222L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long vesGkey;

	@NotNull
	@Size(max=35)
	@Column(name="vesName",nullable=false,length=35,unique=true)
	private String vesName;

	@NotNull
	@Size(max=8)
	@Column(name="vesId",nullable=false,length=8,unique=true)
	private String vesId;

	@NotNull
	@Size(max=8)
	@Column(name="vesLloydsId",nullable=false,length=8,unique=true)
	private String vesLloydsId;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "vesclassGkey")
	private VesselClass vesVesselClass;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "bzuGkey")
	private ScopedBizUnit vesOwner;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "cntryGkey")
	private RefCountry vesCountry;

	@Size(max=35)
	@Column(name="vesCaptain",nullable=true,length=35)
	private String vesCaptain;

	@NotNull
	@Size(max=10)
	@Column(name="vesRadioCallSign",nullable=false,length=10,unique=true)
	private String vesRadioCallSign;

	@NotNull
	@Column(name="vesIsActive",nullable=false)
	private Boolean vesIsActive;

	@NotNull
	@Column(name="vesUnitSystem",nullable=false)
	@Enumerated(EnumType.STRING)
	private UnitSystemEnum vesUnitSystem;

	@NotNull
	@Column(name="vesTemperatureUnits",nullable=false)
	@Enumerated(EnumType.STRING)
	private TempUnitEnum vesTemperatureUnits;

	@Size(max=50)
	@Column(name="vesNotes",nullable=true,length=50)
	private String vesNotes;

	@NotNull
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="vesCreated",nullable=false)
	private Date vesCreated;

	@NotNull
	@Size(max=50)
	@Column(name="vesCreator",nullable=false,length=50)
	private String vesCreator;

	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="vesChanged",nullable=true)
	private Date vesChanged;

	@Size(max=50)
	@Column(name="vesChanger",nullable=true,length=50)
	private String vesChanger;

	@NotNull
	@Column(name="vesStowageScheme",nullable=false)
	@Enumerated(EnumType.STRING)
	private StowageSchemeEnum vesStowageScheme;

	@Size(max=10)
	@Column(name="vesDocumentationNbr",nullable=true,length=10)
	private String vesDocumentationNbr;

	@Size(max=10)
	@Column(name="vesServiceRegistryNbr",nullable=true,length=10)
	private String vesServiceRegistryNbr;

	public Vessel(){}

	public Long getVesGkey() {
		return vesGkey;
	}

	public void setVesGkey(Long vesGkey) {
		this.vesGkey = vesGkey;
	}

	public String getVesName() {
		return vesName;
	}

	public void setVesName(String vesName) {
		this.vesName = vesName;
	}

	public String getVesId() {
		return vesId;
	}

	public void setVesId(String vesId) {
		this.vesId = vesId;
	}

	public String getVesLloydsId() {
		return vesLloydsId;
	}

	public void setVesLloydsId(String vesLloydsId) {
		this.vesLloydsId = vesLloydsId;
	}

	public VesselClass getVesVesselClass() {
		return vesVesselClass;
	}

	public void setVesVesselClass(VesselClass vesVesselClass) {
		this.vesVesselClass = vesVesselClass;
	}

	public ScopedBizUnit getVesOwner() {
		return vesOwner;
	}

	public void setVesOwner(ScopedBizUnit vesOwner) {
		this.vesOwner = vesOwner;
	}

	public RefCountry getVesCountry() {
		return vesCountry;
	}

	public void setVesCountry(RefCountry vesCountry) {
		this.vesCountry = vesCountry;
	}

	public String getVesCaptain() {
		return vesCaptain;
	}

	public void setVesCaptain(String vesCaptain) {
		this.vesCaptain = vesCaptain;
	}

	public String getVesRadioCallSign() {
		return vesRadioCallSign;
	}

	public void setVesRadioCallSign(String vesRadioCallSign) {
		this.vesRadioCallSign = vesRadioCallSign;
	}

	public Boolean getVesIsActive() {
		return vesIsActive;
	}

	public void setVesIsActive(Boolean vesIsActive) {
		this.vesIsActive = vesIsActive;
	}

	public String getVesNotes() {
		return vesNotes;
	}

	public void setVesNotes(String vesNotes) {
		this.vesNotes = vesNotes;
	}

	public Date getVesCreated() {
		return vesCreated;
	}

	public void setVesCreated(Date vesCreated) {
		this.vesCreated = vesCreated;
	}

	public String getVesCreator() {
		return vesCreator;
	}

	public void setVesCreator(String vesCreator) {
		this.vesCreator = vesCreator;
	}

	public Date getVesChanged() {
		return vesChanged;
	}

	public void setVesChanged(Date vesChanged) {
		this.vesChanged = vesChanged;
	}

	public String getVesChanger() {
		return vesChanger;
	}

	public void setVesChanger(String vesChanger) {
		this.vesChanger = vesChanger;
	}

	public String getVesDocumentationNbr() {
		return vesDocumentationNbr;
	}

	public void setVesDocumentationNbr(String vesDocumentationNbr) {
		this.vesDocumentationNbr = vesDocumentationNbr;
	}

	public String getVesServiceRegistryNbr() {
		return vesServiceRegistryNbr;
	}

	public void setVesServiceRegistryNbr(String vesServiceRegistryNbr) {
		this.vesServiceRegistryNbr = vesServiceRegistryNbr;
	}

	public UnitSystemEnum getVesUnitSystem() {
		return vesUnitSystem;
	}

	public void setVesUnitSystem(UnitSystemEnum vesUnitSystem) {
		this.vesUnitSystem = vesUnitSystem;
	}

	public TempUnitEnum getVesTemperatureUnits() {
		return vesTemperatureUnits;
	}

	public void setVesTemperatureUnits(TempUnitEnum vesTemperatureUnits) {
		this.vesTemperatureUnits = vesTemperatureUnits;
	}

	public StowageSchemeEnum getVesStowageScheme() {
		return vesStowageScheme;
	}

	public void setVesStowageScheme(StowageSchemeEnum vesStowageScheme) {
		this.vesStowageScheme = vesStowageScheme;
	}
	
	


}
