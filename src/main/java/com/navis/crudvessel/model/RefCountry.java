package com.navis.crudvessel.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="RefCountry") 
@NamedQueries(value={
		@NamedQuery(name="search.refcountry.all", 
				query="select rc from RefCountry rc"),
		@NamedQuery(name="search.refcountry.by.gkey", 
		query="select rc from RefCountry rc where rc.cntryGkey = :gkey")
})
public class RefCountry implements Serializable{

	
	private static final long serialVersionUID = -1434293271254894363L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cntryGkey;

	@NotNull
	@Size(max=2)
	@Column(name="cntryCode",nullable=false,length=2,unique=true)
	private String cntryCode;
	
	@NotNull
	@Size(max=3)
	@Column(name="cntryAlpha3Code",nullable=false,length=3,unique=true)
	private String cntryAlpha3Code;
	
	@NotNull
	@Size(max=3)
	@Column(name="cntryNum3Code",nullable=false,length=3,unique=true)
	private String cntryNum3Code;
	
	@NotNull
	@Size(max=50)
	@Column(name="cntryName",nullable=false,length=50,unique=true)
	private String cntryName;
	
	@NotNull
	@Size(max=100)
	@Column(name="cntryOfficialName",nullable=false,length=100,unique=true)
	private String cntryOfficialName;
	
	public RefCountry(){}

	public Long getCntryGkey() {
		return cntryGkey;
	}

	public void setCntryGkey(Long cntryGkey) {
		this.cntryGkey = cntryGkey;
	}

	public String getCntryCode() {
		return cntryCode;
	}

	public void setCntryCode(String cntryCode) {
		this.cntryCode = cntryCode;
	}

	public String getCntryAlpha3Code() {
		return cntryAlpha3Code;
	}

	public void setCntryAlpha3Code(String cntryAlpha3Code) {
		this.cntryAlpha3Code = cntryAlpha3Code;
	}

	public String getCntryNum3Code() {
		return cntryNum3Code;
	}

	public void setCntryNum3Code(String cntryNum3Code) {
		this.cntryNum3Code = cntryNum3Code;
	}

	public String getCntryName() {
		return cntryName;
	}

	public void setCntryName(String cntryName) {
		this.cntryName = cntryName;
	}

	public String getCntryOfficialName() {
		return cntryOfficialName;
	}

	public void setCntryOfficialName(String cntryOfficialName) {
		this.cntryOfficialName = cntryOfficialName;
	}
	
	

	
}
