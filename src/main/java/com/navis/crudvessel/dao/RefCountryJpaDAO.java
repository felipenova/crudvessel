package com.navis.crudvessel.dao;

import org.springframework.stereotype.Repository;

import com.navis.crudvessel.model.RefCountry;

/**
 * Class responsible for database manipulation RefCountry.
 * @author Felipe Nova
 *
 */
@Repository
public class RefCountryJpaDAO extends GenericJpaDAO<RefCountry> implements RefCountryDAO {
	

}
