package com.navis.crudvessel.dao;

import org.springframework.stereotype.Repository;

import com.navis.crudvessel.model.VesselClass;

/**
 * Class responsible for database manipulation VesselClass.
 * @author Felipe Nova
 *
 */
@Repository
public class VesselClassJpaDAO extends GenericJpaDAO<VesselClass> implements VesselClassDAO {
	

}
