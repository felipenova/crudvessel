package com.navis.crudvessel.dao;

import org.springframework.stereotype.Repository;

import com.navis.crudvessel.model.ScopedBizUnit;

/**
 * Class responsible for database manipulation ScopedBizUnit.
 * @author Felipe Nova
 *
 */
@Repository
public class ScopedBizUnitJpaDAO extends GenericJpaDAO<ScopedBizUnit> implements ScopedBizUnitDAO {
	

}
