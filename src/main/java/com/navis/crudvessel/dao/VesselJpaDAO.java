package com.navis.crudvessel.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.navis.crudvessel.model.Vessel;

/**
 * Class responsible for database manipulation.
 * @author Felipe Nova
 *
 */
@Repository
public class VesselJpaDAO extends GenericJpaDAO<Vessel> implements VesselDAO {

	@Override
	public Vessel insert(Vessel vessel) throws Exception {
		manager.persist(vessel);
		manager.flush();
		return vessel;
	}

	@Override
	public Vessel update(Vessel vessel) throws Exception {
		Vessel v = manager.merge(vessel);
		manager.flush();
		return v;
	}

	@Override
	public void delete(Vessel vessel) throws Exception {
		manager.remove(vessel);
		manager.flush();
	}

	@Override
	public List<Vessel> searchVesselByIdOrName(String search) throws Exception {
		String namedQuery = "search.vessel.by.id.or.name";
		TypedQuery<Vessel> q = manager.createNamedQuery(namedQuery, Vessel.class);
		q.setParameter("search", search.toUpperCase());
		return q.getResultList();
	}

}
