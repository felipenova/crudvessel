package com.navis.crudvessel.dao;

import com.navis.crudvessel.model.VesselClass;

public interface VesselClassDAO extends GenericDAO<VesselClass> {
	
}
