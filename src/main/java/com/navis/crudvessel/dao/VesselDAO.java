package com.navis.crudvessel.dao;

import java.util.List;

import com.navis.crudvessel.model.Vessel;

public interface VesselDAO extends GenericDAO<Vessel> {
	public Vessel insert(Vessel vessel) throws Exception;
	public Vessel update(Vessel vessel) throws Exception;
	public void delete(Vessel vessel) throws Exception;
	public List<Vessel> searchVesselByIdOrName(String search) throws Exception;
}
