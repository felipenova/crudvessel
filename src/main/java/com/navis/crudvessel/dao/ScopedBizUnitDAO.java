package com.navis.crudvessel.dao;

import com.navis.crudvessel.model.ScopedBizUnit;

public interface ScopedBizUnitDAO extends GenericDAO<ScopedBizUnit> {
	
}
