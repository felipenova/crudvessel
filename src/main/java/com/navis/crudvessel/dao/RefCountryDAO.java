package com.navis.crudvessel.dao;

import com.navis.crudvessel.model.RefCountry;

public interface RefCountryDAO extends GenericDAO<RefCountry> {
	
}
