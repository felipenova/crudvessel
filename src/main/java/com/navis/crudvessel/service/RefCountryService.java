package com.navis.crudvessel.service;

import com.navis.crudvessel.model.RefCountry;

public interface RefCountryService extends GenericService<RefCountry> {
}
