package com.navis.crudvessel.service;

import java.util.List;

import com.navis.crudvessel.model.Vessel;

public interface VesselService extends GenericService<Vessel> {
	public Vessel insert(Vessel vessel) throws Exception;
	public Vessel update(Vessel vessel,Long gkey) throws Exception;
	public void delete(Long gkey) throws Exception;
	public List<Vessel> searchVesselByIdOrName(String search) throws Exception;
}
