package com.navis.crudvessel.service;

import java.util.List;

import com.navis.crudvessel.dao.GenericDAO;

public class GenericServiceImpl<ENT> implements GenericService<ENT>{
	
	
	private GenericDAO<ENT> genericDao;
	
	public GenericServiceImpl(GenericDAO<ENT> genericDao) {
        this.genericDao=genericDao;
    }
 
    public GenericServiceImpl() {
    }

	@Override
	public List<ENT> getAll() throws Exception {
		return genericDao.getAll();
	}

	@Override
	public ENT getByGkey(Long gkey) throws Exception{
		return genericDao.getByGkey(gkey);
	} 
	
	

}
