package com.navis.crudvessel.service;

import com.navis.crudvessel.model.ScopedBizUnit;

public interface ScopedBizUnitService extends GenericService<ScopedBizUnit> {
}
