package com.navis.crudvessel.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.navis.crudvessel.dao.GenericDAO;
import com.navis.crudvessel.model.RefCountry;
import com.navis.crudvessel.model.ScopedBizUnit;

/**
 * Class service responsible for application logic and call the DAO for database manipulation.
 * @author Felipe Nova
 *
 */
@Transactional(readOnly = true)
public class ScopedBizUnitServiceImpl extends GenericServiceImpl<ScopedBizUnit> implements ScopedBizUnitService {

	public ScopedBizUnitServiceImpl(){
 
	}

	@Autowired
	public ScopedBizUnitServiceImpl(
			@Qualifier("scopedBizUnitDAO") GenericDAO<ScopedBizUnit> genericDao) {
		super(genericDao);
	}
}
