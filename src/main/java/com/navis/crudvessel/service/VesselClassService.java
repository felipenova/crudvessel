package com.navis.crudvessel.service;

import com.navis.crudvessel.model.VesselClass;

public interface VesselClassService extends GenericService<VesselClass> {
}
