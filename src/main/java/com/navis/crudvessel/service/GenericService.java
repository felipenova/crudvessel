package com.navis.crudvessel.service;

import java.util.List;

public interface GenericService<ENT> {

	public List<ENT> getAll() throws Exception;
	public ENT getByGkey(Long gkey) throws Exception;
}
