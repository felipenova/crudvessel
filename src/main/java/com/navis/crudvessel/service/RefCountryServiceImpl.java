package com.navis.crudvessel.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.navis.crudvessel.dao.GenericDAO;
import com.navis.crudvessel.model.RefCountry;

/**
 * Class service responsible for application logic and call the DAO for database manipulation.
 * @author Felipe Nova
 *
 */
@Transactional(readOnly = true)
public class RefCountryServiceImpl extends GenericServiceImpl<RefCountry> implements RefCountryService {

	public RefCountryServiceImpl(){

	}

	@Autowired
	public RefCountryServiceImpl(
			@Qualifier("refCountryDAO") GenericDAO<RefCountry> genericDao) {
		super(genericDao);
	}
}
