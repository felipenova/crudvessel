package com.navis.crudvessel.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.navis.crudvessel.dao.GenericDAO;
import com.navis.crudvessel.model.VesselClass;

/**
 * Class service responsible for application logic and call the DAO for database manipulation.
 * @author Felipe Nova
 *
 */
@Transactional(readOnly = true)
public class VesselClassServiceImpl extends GenericServiceImpl<VesselClass> implements VesselClassService {

	public VesselClassServiceImpl(){
 
	}

	@Autowired
	public VesselClassServiceImpl(
			@Qualifier("vesselClassDAO") GenericDAO<VesselClass> genericDao) {
		super(genericDao);
	}
}
