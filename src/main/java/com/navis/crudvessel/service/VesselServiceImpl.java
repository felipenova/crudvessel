package com.navis.crudvessel.service;


import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.navis.crudvessel.dao.GenericDAO;
import com.navis.crudvessel.dao.VesselDAO;
import com.navis.crudvessel.model.RefCountry;
import com.navis.crudvessel.model.ScopedBizUnit;
import com.navis.crudvessel.model.Vessel;
import com.navis.crudvessel.model.VesselClass;

/**
 * Class service responsible for application logic and call the DAO for database manipulation.
 * @author Felipe Nova
 *
 */
@Transactional(readOnly = true)
public class VesselServiceImpl extends GenericServiceImpl<Vessel> implements VesselService {

	private static final Logger logger = LoggerFactory.getLogger(VesselServiceImpl.class);

 
	@Autowired
	private RefCountryService refCountryService;
	
	@Autowired
	private VesselClassService vesselClassService;
	
	@Autowired
	private ScopedBizUnitService scopedBizUnitService;
	
	private VesselDAO vesselDAO;
	
	
	public VesselServiceImpl(){
		 
    }
	
    @Autowired
    public VesselServiceImpl(
            @Qualifier("vesselDAO") GenericDAO<Vessel> genericDao) {
        super(genericDao);
        this.vesselDAO = (VesselDAO) genericDao;
    }

	/**
	 * Responsible method to call the DAO method that makes the inclusion in the database .
	 * @param em
	 * @return Vessel
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = { Exception.class },readOnly = false)
	public Vessel insert(Vessel vessel) throws Exception{
		try {
			setRelationships(vessel);
			vessel.setVesCreated(new Date());
			vessel.setVesCreator("zilics");
			vessel.setVesIsActive(true);
			return vesselDAO.insert(vessel);
		} catch(Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public Vessel update(Vessel vessel, Long gkey) throws Exception {
		try {
			validatePassedGkey(vessel, gkey);
			Vessel v = vesselDAO.getByGkey(gkey);
			setRelationships(vessel);
			vessel.setVesCreated(v.getVesCreated());
			vessel.setVesCreator(v.getVesCreator()); 
			vessel.setVesChanged(new Date());
			vessel.setVesChanger("zilics");
			return vesselDAO.update(vessel);
		} catch(Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	@Override
	public void delete(Long gkey) throws Exception {
		try {
			Vessel v = vesselDAO.getByGkey(gkey);
			vesselDAO.delete(v);
		} catch(Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	@Override
	public List<Vessel> searchVesselByIdOrName(String search) throws Exception{
		return vesselDAO.searchVesselByIdOrName(search);
	}
	
	private void validatePassedGkey(Vessel vessel, Long gkey) throws Exception{
		if(vessel.getVesGkey() != gkey){
			throw new Exception("A gkey informada n�o � igual a gkey do Navio, e n�o pode ser alterada.");
		}
	}
	
	private void setRelationships(Vessel vessel) throws Exception{
		RefCountry refCountry = refCountryService.getByGkey(vessel.getVesCountry().getCntryGkey());
		if(refCountry != null){
			vessel.setVesCountry(refCountry);
		}
		VesselClass vesselClass = vesselClassService.getByGkey(vessel.getVesVesselClass().getVesclassGkey());
		if(vesselClass != null){
			vessel.setVesVesselClass(vesselClass);
		}
		ScopedBizUnit scopedBizUnit = scopedBizUnitService.getByGkey(vessel.getVesOwner().getBzuGkey());
		if(scopedBizUnit != null){
			vessel.setVesOwner(scopedBizUnit);
		}
	}
	
	

}
