package com.navis.crudvessel.vesellenum;

public enum BizRoleEnum {
	LINEOP, VESSELOP, RAILROAD, HAULIER, SHIPPER, AGENT, MISC, STEVEDORE, SURVEYOR, DCOP, DEPOT, CUSTOMS, GOVAGENCY, FRGTHFWDER, LEASINGCO, TERMINAL;
}
