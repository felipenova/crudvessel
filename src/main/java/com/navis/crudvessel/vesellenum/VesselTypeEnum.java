package com.navis.crudvessel.vesellenum;

public enum VesselTypeEnum {
	CELL, BARGE, BBULK, BULK, RORO, PSNGR, UNKNOWN;
}
