package com.navis.crudvessel.vesellenum;

public enum TempUnitEnum {
	FAHRENHEIT, CELSIUS;
}
