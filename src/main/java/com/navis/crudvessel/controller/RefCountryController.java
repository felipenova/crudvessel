package com.navis.crudvessel.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.navis.crudvessel.model.RefCountry;
import com.navis.crudvessel.service.RefCountryService;

/**
 * 
 * @author Felipe Nova
 * Class responsible for exposing the rest layer
 *
 */
@Controller
@RequestMapping("/rest")
public class RefCountryController {

	private static final Logger logger = LoggerFactory.getLogger(RefCountryController.class);

	@Autowired
	private RefCountryService refCountryService; 
	
	private Gson gson = new Gson(); 
	
	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getAllCountries(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			List<RefCountry> list = refCountryService.getAll();
			response = gson.toJson(list);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/countries/{gkey}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getCountryByGkey(@PathVariable("gkey") Long gkey,HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			RefCountry country = refCountryService.getByGkey(gkey);
			response = gson.toJson(country);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	

	

}
