package com.navis.crudvessel.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.navis.crudvessel.model.Vessel;
import com.navis.crudvessel.service.VesselService;

/**
 * 
 * @author Felipe Nova
 * Class responsible for exposing the rest layer
 *
 */
@Controller
@RequestMapping("/rest")
public class VesselController {

	private static final Logger logger = LoggerFactory.getLogger(VesselController.class);

	@Autowired
	private VesselService vesselService;
	
	private Gson gson = new Gson();
	
	/**
	 * Method responsible for receiving the request and call the method that will make the integration of data.
	 * @param vessel
	 * @param request
	 * @return ResponseEntity<String>
	 */
	@RequestMapping(value = "/vessels", method = RequestMethod.POST)
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	@Consumes(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> insertVessel(@RequestBody Vessel vessel,HttpServletRequest request){	
		try{
			vesselService.insert(vessel);
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/vessels/{gkey}", method = RequestMethod.PUT)
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	@Consumes(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateVessel(@RequestBody Vessel vessel,@PathVariable("gkey") Long gkey,HttpServletRequest request){	
		try{
			vesselService.update(vessel,gkey);
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/vessels/{gkey}", method = RequestMethod.DELETE)
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	@Consumes(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteVessel(@PathVariable("gkey") Long gkey,HttpServletRequest request){	
		try{
			vesselService.delete(gkey);
			return new ResponseEntity<String>("Registro deletado", HttpStatus.OK);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/vessels", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getAllVessels(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			List<Vessel> list = vesselService.getAll();
			response = gson.toJson(list);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/vessels/{gkey}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getVesselByGkey(@PathVariable("gkey") Long gkey,HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			Vessel vessel = vesselService.getByGkey(gkey);
			response = gson.toJson(vessel);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/vesselsbyidorname", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getVesselByIdOrName(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		String search 		= request.getParameter("search");
		try{
			List<Vessel> vessels = vesselService.searchVesselByIdOrName(search);
			response = gson.toJson(vessels);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	

}
