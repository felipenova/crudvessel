package com.navis.crudvessel.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.navis.crudvessel.model.VesselClass;
import com.navis.crudvessel.service.VesselClassService;

/**
 * 
 * @author Felipe Nova
 * Class responsible for exposing the rest layer
 *
 */
@Controller
@RequestMapping("/rest")
public class VesselClassController {

	private static final Logger logger = LoggerFactory.getLogger(VesselClassController.class);

	@Autowired
	private VesselClassService vesselClassService; 
	
	private Gson gson = new Gson(); 
	
	@RequestMapping(value = "/vesselclasses", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getAllVesselClasses(HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			List<VesselClass> list = vesselClassService.getAll();
			response = gson.toJson(list);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/vesselclasses/{gkey}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getVesselClassByGkey(@PathVariable("gkey") Long gkey,HttpServletRequest request, HttpServletResponse resp) {
		String response = "";
		try{
			VesselClass vc = vesselClassService.getByGkey(gkey);
			response = gson.toJson(vc);
		}catch(Exception e){
			logger.error(e.getMessage());
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	

	

}
